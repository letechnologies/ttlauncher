#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from AMSpi import AMSpi
import time

if __name__ == '__main__':
    # Calling AMSpi() we will use default pin numbering: BCM (use GPIO numbers)
    # if you want to use BOARD numbering do this: "with AMSpi(True) as amspi:"
    with AMSpi() as amspi:
        # Set PINs for controlling shift register (GPIO numbering)
        amspi.set_74HC595_pins(21, 20, 16)
        # Set PINs for controlling all 4 motors (GPIO numbering)
        amspi.set_L293D_pins(5, 6, 13, 19)
        print("GO: clockwise")
        amspi.run_dc_motor (amspi.DC_Motor_4, speed=5)
        time.sleep(120)
        print("Stop")
        amspi.stop_dc_motors([amspi.DC_Motor_4])
