#!/usr/bin/env python3

import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

_DIR_LATCH = 21
_DIR_CLK = 20
_DIR_SER = 16

GPIO.setup(_DIR_LATCH, GPIO.OUT)
GPIO.setup(_DIR_CLK, GPIO.OUT)
GPIO.setup(_DIR_SER, GPIO.OUT)

pin1 = 5
pin2 = 6
pin3 = 13
pin4 = 19

GPIO.setup(pin1, GPIO.OUT)
GPIO.setup(pin2, GPIO.OUT)
GPIO.setup(pin3, GPIO.OUT)
GPIO.setup(pin4, GPIO.OUT)

GPIO.output(_DIR_CLK, GPIO.LOW)

GPIO.output(_DIR_SER, GPIO.LOW)
GPIO.output(_DIR_SER, GPIO.HIGH)
GPIO.output(_DIR_SER, GPIO.LOW)
GPIO.output(_DIR_SER, GPIO.LOW)
GPIO.output(_DIR_SER, GPIO.LOW)
GPIO.output(_DIR_SER, GPIO.LOW)
GPIO.output(_DIR_SER, GPIO.HIGH)
GPIO.output(_DIR_SER, GPIO.LOW)



GPIO.output(_DIR_LATCH, GPIO.HIGH)
GPIO.output(pin1, GPIO.HIGH)
GPIO.output(pin2, GPIO.HIGH)
GPIO.output(pin3, GPIO.HIGH)
GPIO.output(pin4, GPIO.HIGH)

time.sleep(5)

GPIO.output(pin1, GPIO.LOW)
GPIO.output(pin2, GPIO.LOW)
GPIO.output(pin3, GPIO.LOW)
GPIO.output(pin4, GPIO.LOW)

#GPIO.cleanup()