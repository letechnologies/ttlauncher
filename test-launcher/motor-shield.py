#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BOARD)

Motor1 = 16    # Input Pin
Motor2 = 18    # Input Pin
Motor3 = 22    # Enable Pin

GPIO.setup(Motor1,GPIO.OUT)
GPIO.setup(Motor2,GPIO.OUT)
GPIO.setup(Motor3,GPIO.OUT)

p = GPIO.PWM(Motor3, 100)
p.start(100)

print ("FORWARD MOTION")
GPIO.output(Motor1,GPIO.HIGH)
GPIO.output(Motor2,GPIO.LOW)
GPIO.output(Motor3,GPIO.HIGH)
 
sleep(5)

p.ChangeDutyCycle(90)
sleep(5)

p.ChangeDutyCycle(80)
sleep(5)

p.ChangeDutyCycle(70)
sleep(5)

p.ChangeDutyCycle(60)
sleep(5)

p.ChangeDutyCycle(50)
sleep(5)

p.ChangeDutyCycle(40)
sleep(5)

p.ChangeDutyCycle(30)
sleep(5)

p.ChangeDutyCycle(20)
sleep(5)

p.ChangeDutyCycle(10)
sleep(5)

p.ChangeDutyCycle(0)
sleep(5)

p.ChangeDutyCycle(10)
sleep(5)

p.ChangeDutyCycle(20)
sleep(5)

p.ChangeDutyCycle(30)
sleep(5)

p.ChangeDutyCycle(40)
sleep(5)

print ("BACKWARD MOTION")



GPIO.output(Motor1,GPIO.LOW)
GPIO.output(Motor2,GPIO.HIGH)
GPIO.output(Motor3,GPIO.HIGH)
 
sleep(20)
 
print ("STOP")
GPIO.output(Motor1,GPIO.LOW)
GPIO.output(Motor2,GPIO.LOW)
GPIO.output(Motor3,GPIO.LOW)


p.stop() 
GPIO.cleanup()