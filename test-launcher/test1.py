#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time

pin = 18
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin, GPIO.OUT) ## GPIO como salida

def parpadear():
    tiempoEspera = 0.125
    for i in range(300):
        GPIO.output(pin, True)
        time.sleep(tiempoEspera) ## Esperamos
        GPIO.output(pin, False)
        time.sleep(tiempoEspera) ## Esperamos
    GPIO.cleanup() ## Hago una limpieza de los GPIO
    print("Terminado")

parpadear()
