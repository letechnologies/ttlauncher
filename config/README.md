## Archivos de configuracion (*EXPLICACION*)

**Glosario**

*CL - Cabezal Lanzador*

1. Datos iniciales de los motores para el lanzamiento.
2. Explicacion de algunos datos preliminares

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Archivo (data.json) y sus atributos
*Este archivo tiene la lista de lanzamientos por pelota que va a hacer cabezal lanzador*

Atributos de cada elemento de la lista:

1. *power* sera un valor entre 1 - 10, y es para indicarle al CL la fuerza con la que debe impulsar la pelota, valor por defecto 1.
2. *effect* sera un id, ver lista **effects-list.json**, y es para indicarle al CL el efecto que debe imprimirle a la pelota en el lanzamiento, es obligatorio.
3. *angle* sera un valor entre 0 - 45, para indicar el angulo que va a tener el efecto (*effect*), valor por defecto 0.
4. *time* sera un valor entre 1 - 20 (segundos), para indicar el tiempo en segundos entre lanzamientos, valor por defecto 1.

---